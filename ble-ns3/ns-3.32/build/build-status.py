#! /usr/bin/env python3

# Programs that are runnable.
ns3_runnable_programs = ['build/scratch/ns3.32-ble-data-trace-generator-debug', 'build/scratch/ns3.32-scratch-simulator-debug', 'build/scratch/subdir/ns3.32-subdir-debug', 'build/scratch/ns3.32-ble-mixed-trace-generator-debug', 'build/scratch/ns3.32-ble-public-trace-generator-debug']

# Scripts that are runnable.
ns3_runnable_scripts = []

