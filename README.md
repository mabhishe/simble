***Realistic BLE Simulator in NS-3 framework***

> **SimBle is the first BLE simulation stack capable of generating traces that preserve privacy. It introduces resolvable private addresses that are the core to BLE device and network privacy-provisions. We showed that it is capable of emulating the behavior of any real BLE device/hardware. Users have to choose the appropriate device class they want to test, based on the targeted device. It resolved the lack of ground truth for scalable scenarios after the introduction of MAC address randomization. SimBle provides the associated ground truth with every trace that is generated.**

Bluetooth has become critical as many IoT devices are arriving in the market. Most of the current literature focusing on Bluetooth simulation concentrates on the network protocols’ performances and completely neglects the privacy protection recommendations introduced in the BLE standard.

Indeed, privacy protection is one of the main issues handled in the Bluetooth standard. For instance, the current standard forces devices to change the identifier they embed within the public and private packets, known as MAC address randomization. It is indispensable for protecting user-privacy in BLE as we see in Section II. If devices keep on advertising their true MAC address or their Identity Address, they could easily be tracked by co-coordinated passive sniffing. Widespread usage of resolvable private addresses could potentially protect the privacy of users to some extent. On the other side, vendor-dependent MAC address randomization has lead to the retrieval of realistic BLE traces more and more challenging. The lack of ground truth in randomized traces and impracticality of large-scale passive trace collection is making the testing of solutions based on device fingerprinting. All of the existing and future works based on device identification using MAC address in BLE must be revisited with the introduction of BLE privacy-provisions like private addresses. 

Unfortunately, existing evaluation tools such as NS-3 are not designed to reproduce this Bluetooth standard’s essential functionality. This makes it impossible to test solutions for different device-fingerprinting strategies as there is a lack of ground truth for large-scale scenarios with the majority of
current BLE devices implementing MAC address randomization. Here, we first introduce a solution of standard-compliant MAC address randomization in the NS-3 framework, capable of emulating any real BLE device in the simulation and generating real-world Bluetooth traces. In addition, since the simulation runtime for trace-collection grows exponentially with the number of devices, we introduce an optimization to linearize public-packet sniffing. This made the large-scale trace-collection practically feasible. 

SimBle is the answer to this issue as researchers could now generate large-scale trace traces with devices of their interest and use it to validate their works. Sniffers could be deployed accordingly to emulate real-world passive tracecollection for BLE. The works that do BLE MAC address association or devicefingerprinting are threats to privacy provisions of BLE as these strategies lead to tracking of users. Only SimBle can allow the community to compare the effectiveness of any two of these available solutions. This is because we need exact/identical conditions for comparing the evaluations. It is not only hard for experiments/test-beds to emulate identical conditions but are also not scalable. Moreover, as discussed earlier, finding ground truth for experimentally obtained traces is practically impossible for large-scale testing.

> *Regarding future contribution to this work, the key distribution could be done by using control messages rather than pre-installation at the node. BLE stack could be enriched by the addition of different device pairing modes. Also, as one of the aims of SimBle is to emulate any real device, more and more vendor-specific information could be added to facilitate usability. Any contributions are welcome.*


> **Please do cite the base paper: https://hal.archives-ouvertes.fr/hal-03217312/, if you use *SimBle* for your work, using the BibTeX:** \
@inproceedings{mishra:hal-03217312,
  TITLE = {{SimBle: Comment g{\'e}n{\'e}rer des traces r{\'e}elles Bluetooth conformes aux recommandations de pr{\'e}servation de la vie priv{\'e}e ?}},
  AUTHOR = {Mishra, Abhishek K and Viana, Aline C and Achir, Nadjib},
  URL = {https://hal.archives-ouvertes.fr/hal-03217312},
  BOOKTITLE = {{ALGOTEL 2021 - 23{\`e}mes Rencontres Francophones sur les Aspects Algorithmiques des T{\'e}l{\'e}communications}},
  ADDRESS = {La Rochelle, France},
  YEAR = {2021},
  MONTH = Jun,
  KEYWORDS = {Bluetooth ; randomisation d'adresses MAC ; NS-3 ; pr{\'e}servation de la vie priv{\'e}e},
  PDF = {https://hal.archives-ouvertes.fr/hal-03217312/file/SimBle__Comment_g_n_rer_des_traces_r_elles_Bluetooth_conformes_aux_recommandations_de_pr_servation_de_la_vie_priv_e__.pdf},
  HAL_ID = {hal-03217312},
  HAL_VERSION = {v1},
}

*Authors:* 
1. Abhishek Kumar MISHRA, Inria Saclay
2. Aline Carneiro VIANA, Inria Saclay
3. Nadjib ACHIR, Inria, Inria Saclay


> **Usage info**

After cloning the repository please build the SimBle, using the following command in the ble-ns3/ directory: 
`CXXFLAGS="-W -Wall -g" ./build.py --disable-netanim`

*To collect BLE data-traffic traces:*\
    *Visit* -> ble-ns3/ns-3.32/src/ble/scratch/ble-data-trace-generator.cc\
    *Example usage(Command from directory: ble-ns3/ns-3.32/)* -> NS_LOG="BleData" ./waf --run "scratch/ble-data-trace-generator --mobility_profile=0 --device_class=0 --num_devices=2 --rand_invl=30"

*To collect BLE public-traffic traces:*\
    *Visit* -> ble-ns3/ns-3.32/src/ble/scratch/ble-public-trace-generator.cc\
    *Example usage(Command from directory: ble-ns3/ns-3.32/)* -> NS_LOG="BleBroadcast" ./waf --run "scratch/ble-broadcast --mobility_profile=0 --device_class=1 --num_devices=5 --num_sniffers=1 --rand_invl=900"

*Where the arguments are:*
1. *mobility_profile* - Mobility profile(refer paper for details on profiles)
2. *device_class* - 0 for just mobile phones, 1 for all kinds of BLE devices
3. *num_devices* - number of BLE nodes
4. *num_sniffers* - number of sniffers deployed
5. *rand_invl* - BLE randomization interval (don't mention it if you want standard defaults)

The generated traces could be found in the directory: ns-3.32/traces/

## If facing any difficulties, please feel free to contact on email: abhishek.mishra@inria.fr
